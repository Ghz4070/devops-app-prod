FROM node:14-alpine AS base

WORKDIR /app
EXPOSE 3000
CMD npm ci && npm run server:dev

FROM base
COPY package*.json /app

RUN npm ci

COPY . /app

CMD npm run start