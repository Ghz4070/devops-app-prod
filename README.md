# API Express + Node + Postgres

```
app/
  config/
  migrations/
  scripts/
  src-server/
  package.json
  README.md
  .env.example
  .gitignore
```

> **Vous n'aurez à modifier aucun code existant de l'application afin de réaliser ce projet !**

## Pré-requis

Vous aurez besoin des versions suivantes pour lancer le projet

- [node v8 ou plus](https://nodejs.org/en/download/)
- npm v5 ou plus
- [PostgreSQL](https://www.postgresql.org/download/)

## Installation

> **Vous n'aurez à modifier aucun code existant de l'application afin de réaliser ce projet !**

1. Lancez `npm install` une fois dans le dossier.
2. Créez `.env` et définissez ses valeurs en prennant comme example le contenu de `.env.example`
3. Lancez les seed de votre application `npm run seed` (nécessite d'avoir une BDD dispo)
4. Démarrez l'enrivonnement de dev `npm run server:dev`
5. Ou bien démarrez l'environnement de prod `npm run server:prod` ou bien avec `npm run start`

### Scripts à disposition

- `$ npm run start`: Démarrer l'environnement de prod
- `$ npm run server:dev`: Démarrer l'environnement en mode développement
- `$ npm run lint`: Lancer le lint de code
- `$ npm run seed`: Lancer les migrations et seeds de la base de données
- `$ npm run test:server`: Lancer les tests
- `$ npm run db:migrate`: Lancer les migrations

## Installation avec Docker

https://www.digitalocean.com/community/tutorials/how-to-build-a-node-js-application-with-docker-quickstart-fr
https://nodejs.org/fr/docs/guides/nodejs-docker-webapp/

## API Endpoints (/api/\*)

### `GET /api/posts`: Get all Posts

Récupère la liste des posts existants en base de données.

Si le code HTTP "200" apparaît ou bien "ok" dans la réponse c'est que la requête est réussie et donc que l'application fonctionne correctement.

Format de la réponse :

```
let response = {
    statusCode: 200,
    body: [
            Post1,
            Post2,
            ...
            PostN
        ]
    }
```

### `GET /api/posts/:id`: Get a Post by ID

Récupère un post en particulier en remplaçant l'id de l'url par celui d'un post existant.

### `DELETE /api/posts/:id`: Delete a Post by ID

Permet de supprimer un post en particulier en remplaçant l'id par celui d'un post existant

---

Application qui ne fait rien de spécial mais c'est une API + BDD

- Readme explicant comment installer le projet.
- "npm install --force" car depuis le temps des libs sont dépreciées mais on s'en fou pour ce projet.

Ensuite :

- [ ] Créer l'environnement de développement pour les développeurs de l'application (docker-compose)
- [ ] Créer les images de prod de l'application
- [ ] Création d'une pipeline CI/CD gitab-ci qui permet de faire toute la CI de l'application (jusqu'à la création de l'image Docker dans le registre Docker de votre compte Gitlab)
- [ ] Déployer avec Kubernetes (au format code) l'image de votre application récupérée de votre registre Docker Gitlab dans un cluster "minikube" que vous avez en local (gratuit) ou si possible sur un cluster Kubernetes Managé online créé avec Terraform.

Pensez à :

- [ ] Lancer des tests / linters dans la pipeline et potentiellement dans les hook git

Donc en gros, tu dois dockerizer l'application en faisant son Dockerfile, créer et push l'image sur un registry puis déployer ton appli sur kubernetes avec 2 services (api & bdd). A toi de voir si tu veux le faire sur minikube ou sur le cloud.

---

## BONUS

Challenge : change ça pour injecter le password depuis un fichier avec docker secrets
const connectionStr = `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${hostWithPort}/${process.env.DB_NAME}`;

en gros tu dois utiliser fs pour lire le fichier process.env.DB_PASSWORD_FILE
avec DB_PASSWORD_FILE = /run/secrets/database-password
ouai fs c'est la lib de filesystem
